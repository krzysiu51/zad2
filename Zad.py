#!/usr/bin/env python3
class Plant:
    pass


class Flower(Plant):
    pass


class Orchid(Flower):
    pass


class Tree(Plant):
    pass


class Maple(Tree):
    pass


orchid = Orchid()
maple = Maple()


print(orchid)
print(isinstance(orchid, Orchid))
print(isinstance(orchid, Flower))
print(isinstance(orchid, Plant))
print(isinstance(orchid, object))

print(maple)
print(isinstance(maple, Maple))
print(isinstance(maple, Tree))
print(isinstance(maple, Plant))
print(isinstance(maple, object))

print(isinstance(maple, Flower))
print(isinstance(orchid, Tree))

